from InstagramAPI import InstagramAPI
import os
import time

def get_last_posts(self, usernameId, numOfPosts, minTimestamp=None):
	user_feed = []
	next_max_id = ''
	while True:
		self.getUserFeed(usernameId, next_max_id, minTimestamp)
		temp = self.LastJson
		for item in temp["items"]:
			if numOfPosts > 0:
				user_feed.append(item)
				numOfPosts = numOfPosts - 1
			else:
				break
		
		return user_feed
			
def comment_last_user_posts(api, username, comment, numOfPosts):
	usernameId = getUserID(api, username)
	posts = get_last_posts(api, usernameId, numOfPosts)
	res = 0
	
	for item in posts:
		res = api.comment(item["id"], comment)
	
	return res
	
def like_last_user_posts(api, username, numOfPosts):
	usernameId = getUserID(api, username)
	posts = get_last_posts(api, usernameId, numOfPosts)
	res = 0
	
	for item in posts:
		res = api.like(item["id"])
	
	return res
		
def getUserID(api, username):
	api.searchUsername(username)
	try:
		return api.LastJson["user"]["pk"]
	except:
		print("Couldn't get user ID")

def unfollow_everybody(self, myUsername):
	users_to_unfollow = self.getTotalFollowings(getUserID(self, myUsername))
	num_of_user = 1
	
	for user in users_to_unfollow:
		print(num_of_user, ". Unfollowing " + user["username"])
		self.unfollow(getUserID(self, user['username']))
		num_of_user += 1
		print("Waiting 10 seconds until next engagement...")
		time.sleep(10)

def smart_unfollow(self, myUsername, previous_followings):
	current_followings = self.getTotalFollowings(getUserID(self, myUsername))
	num_of_user = 1
	
	for user in current_followings:
		if user["username"] not in previous_followings:
			print(num_of_user, ". Unfollowing " + user["username"])
			self.unfollow(getUserID(self, user['username']))
			num_of_user += 1
			print("Waiting 10 seconds until next engagement...")
			time.sleep(10)
		
def save_who_i_follow(self, path):
	following_list = self.getTotalSelfFollowings()
	following_usernames_list = list()
	
	for user in following_list:
		following_usernames_list.append(user["username"])
		
	file = open(path, 'w')
	for username in following_usernames_list:
		file.write(username)
		file.write("\n")
		
	file.close()

def load_previous_followings(path):
	file = open(path, 'r')
	
	following_usernames_list = file.read()
	following_usernames_list = following_usernames_list.split("\n")
	following_usernames_list.remove("")
	
	file.close()
	return following_usernames_list
	
def main_marketing(self, account_to_view, numOfPostsToLike, numOfPostsToComment, comment, user_blacklist, to_follow):
	usernameId = getUserID(self, account_to_view)
	followers = []
	next_max_id = ''
	num_of_user = 1
	like_res = 0
	comment_res = 0
	follow_res = 0
	
	while 1:
		self.getUserFollowers(usernameId, next_max_id)
		temp = self.LastJson

		for item in temp["users"]:
			if item["username"] not in user_blacklist:
				print(num_of_user, ". Engaging " + item["username"])
				followers.append(item)
				
				try:
					if to_follow:
						print("Following...")
						follow_res = api.follow(getUserID(api, item["username"]))
						
					print("Liking...")
					like_res = like_last_user_posts(api, item["username"], numOfPostsToLike)
					print("Commenting...")
					comment_res = comment_last_user_posts(api, item["username"], comment, numOfPostsToComment)
					
					print()
					"""if (like_res is False) or (comment_res is False) or (follow_res is False):
						print("sleeping since " + time.asctime())
						time.sleep(3600)
						print()"""
						
					num_of_user += 1
					print("Waiting 10 seconds until next engagement...")
					time.sleep(10)
					
				except KeyError:
					print("Not authorized to view user, skipping...\n")
					
		if temp["big_list"] is False:
			return followers
		next_max_id = temp["next_max_id"]

		
#	M A I N  S T A R T	
os.system('cls')
followings_path = "followings.txt"
		
username = input("Enter Username: ")
password = input("Enter Password: ")

api = InstagramAPI(username, password)

if(api.login()):
	os.system('cls')
	#to_unfollow_everybody = int(input("Would you like to unfollow everybody before starting marketing your account? (1 for yes/0 for no): "))
	to_smart_unfollow = int(input("Would you like to unfollow the users you followed last time (if you chose to, if you didn't; enter 0) before starting marketing your account? (1 for yes/0 for no): "))
	
	if to_smart_unfollow == 1:
		print("Starting unfollowing...\n")
		smart_unfollow(api, username, load_previous_followings(followings_path))
		
	os.system('cls')
	
	if to_smart_unfollow == 1:
		print("Unfollowing DONE! Let's start marketing.\n\n")
		
	"""if to_unfollow_everybody == 1:
		print("Starting unfollowing...\n")
		unfollow_everybody(api, username)
		
	os.system('cls')
	
	if to_unfollow_everybody == 1:
		print("Unfollowing DONE! Let's start marketing.\n\n")"""
		
	account_to_view = input("What account's followers will be the kind of followers that you want? (Enter the account name): ")
	num_of_posts_to_like = int(input("How many posts would you want to like per user? "))
	num_of_posts_to_comment = int(input("How many posts would you want to comment per user? "))
	comment = input("What would you like to comment for user's post/s? ")
	#comment = "👌🏼💯"
	
	print("Getting ready to market your account...")
	imFollowing = api.getTotalSelfFollowings()
	final_blacklist = list()
	
	for item in imFollowing:
		final_blacklist.append(item["username"])
	
	to_follow = bool(int(input("Would you like to follow users as an addition to the regular marketing strategy? (1 for yes/0 for no): ")))
	
	save_who_i_follow(api, followings_path)
	
	os.system('cls')
	print("Starting Marketing...")
	main_marketing(api, account_to_view, num_of_posts_to_like, num_of_posts_to_comment, comment, final_blacklist, to_follow)
	
	api.logout()
else:
	print("Couldn't Login")			

